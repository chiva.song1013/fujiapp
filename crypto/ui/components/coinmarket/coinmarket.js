import { Template } from 'meteor/templating';
 
import './coinmarket.html';
// import '../../pages/details/details.html';
 import { HTTP } from 'meteor/http';

HTTP.get('https://api.coinpaprika.com/v1/ticker', {}, function( error, response ) {

   if ( error ) {
      console.log( error );
   } else {
   		var data=[];
   		var i=0;
   		while(i<=response['data'].length){
   			console.log(i + " " + response['data'][i]['name']);
   			i++;
   		}
   }

});

Template.coinmarket.helpers({
  coinList: [
      { _id: 1, logo: 'bitcoin.png', symbol: 'BTC', coin: 'Bitcoin', price: '$6528.19'},
	  { _id: 2, logo: 'ethereum.png', symbol: 'ETH', coin: 'Ethereum', price: '$221.76' },
	  { _id: 3, logo: 'xrp.png', symbol: 'XRP', coin: 'XRP', price: '$0.43' },
	  { _id: 4, logo: 'bitcoin-cash.png', symbol: 'BCH', coin: 'Bitcoin Cash', price: '$0.45' },
	  { _id: 5, logo: 'eos.png', symbol: 'EOS', coin: 'EOS', price: '$5.69' },
	  { _id: 6, logo: 'stellar.png', symbol: 'XLM', coin: 'Stellar', price: '$0.23' },
	  { _id: 7, logo: 'litecoin.png', symbol: 'LTC', coin: 'Litecoin', price: '$56.99' },
	  { _id: 8, logo: 'tether.png', symbol: 'USDT', coin: 'Tether', price: '$0.99' },
	  { _id: 9, logo: 'cardano.png', symbol: 'ADA', coin: 'cardano', price: '$0.083' },
	  { _id: 10, logo: 'monero.png', symbol: 'XMR', coin: 'Monero', price: '$117.76' },
  ],
});